import React from 'react';
import {Uploader, ProfileUploader, ProviderLogin, Spinner, FormModel} from '@inrupt/solid-react-components';
import {LoggedIn, LoggedOut, Value, LoginButton, LogoutButton, Image} from '@solid/react';
import './App.css';
import './index.css';

export default function App() {
  return(
    <div>
      <LoggedOut>
      <div className='loggedout'>
        <h2>Please login:</h2>
        <ProviderLogin callbackUri={`${window.location.origin}/`} />

        <h2>Alternative login:</h2>
        <LoginButton popup="popup.html"/>
      </div>
      </LoggedOut>

      <LoggedIn>
        <div className='loggedin'>
          <h1>You're now logged in <Value src='user.name'/>!</h1>
          <p>This is your picture:</p>
          <Image src='user.vcard_hasPhoto'/>

          <h2>Data pulled from users pod:</h2>
          <p>You live in <Value src='user.vcard_hasAddress.vcard_region'/></p>

          <h2>Edit your name:</h2>
          <FormModel
            {...{
              modelSource: 'https://solidsdk.inrupt.net/public/FormLanguage/examples/FormModel/SingleLineTextField.ttl#formRoot',
              dataSource: 'https://nikosiltaloppi.solid.community/profile/card#me',
              options: {
                theme: {
                  inputText: 'sdk-input',
                },
                autosaveIndicator: Spinner,
                autosave: true
              }
            }}
          />

          <h2>Upload a file:</h2>
          <Uploader
            {...{
              fileBase: 'https://nikosiltaloppi.solid.community/private/uploadtest/',
              render: props => <ProfileUploader {...{ ...props }} />
            }}
          />
          
          <h2>Log out:</h2>
          <LogoutButton/>
        </div>
      </LoggedIn>
    </div>
  );
}
